export default function Tags({ tags = [] }) {
  return (
    <div className="tags are-medium">
      {tags.map((tag, i) => (
        <span key={i} className="tag is-large">
          #{tag}
        </span>
      ))}
    </div>
  );
}
